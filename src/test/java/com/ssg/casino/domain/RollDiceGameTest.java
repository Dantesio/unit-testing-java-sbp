package com.ssg.casino.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class RollDiceGameTest {

    RollDiceGame game = new RollDiceGame();
    List<Player> players = List.of(
            new Player(),
            new Player(),
            new Player(),
            new Player(),
            new Player(),
            new Player()
    );

    public static int ANY_AMOUNT_OF_CHIPS = 10;

    @BeforeEach
    public void setUp() throws CasinoGameException {

        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            player.joins(game);
            player.buy(ANY_AMOUNT_OF_CHIPS);
            player.bet(new Bet(player.getAvailableChips(), i + 1));
        }
    }

    @Test
    public void sixPlayers_play_onePlayerWin6X() {
        game.play();

        List<Player> winners = players.stream().filter(
                        player -> player.getAvailableChips() == 6 * ANY_AMOUNT_OF_CHIPS)
                .collect(Collectors.toList());

        assertEquals(1, winners.size());
        assertEquals(6 * ANY_AMOUNT_OF_CHIPS, winners.get(0).getAvailableChips());
    }
}