package com.ssg.casino.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {

    Player player = new Player();
    RollDiceGame game = new RollDiceGame();

    @Test
    public void shouldJoinGame() throws CasinoGameException {
        player.joins(game);

        assertEquals(game, player.activeGame());
    }

    @Test
    public void canJoinOnlyOneGame() throws CasinoGameException {
        RollDiceGame game2 = new RollDiceGame();

        player.joins(game);

        assertThrows(CasinoGameException.class, () -> player.joins(game2));
    }

    @Test
    public void canJoinSameGame() throws CasinoGameException {
        player.joins(game);
        player.joins(game);

        assertEquals(game, player.activeGame());
    }

    @Test
    public void shouldBuyChips() throws CasinoGameException {
        player.joins(game);
        int anyCountOfChips = 10;

        assertEquals(0, player.getAvailableChips());

        player.buy(anyCountOfChips);

        assertEquals(anyCountOfChips, player.getAvailableChips());
    }

    @Test
    public void shouldNotBuyNegativeChips() throws CasinoGameException {
        player.joins(game);

        assertThrows(CasinoGameException.class, () -> player.buy(-1));
    }

    @Test
    public void shouldBuyChipsMultipleTimes() throws CasinoGameException {
        player.joins(game);
        int chips1 = 10;
        int chips2 = 7;

        player.buy(chips1);
        player.buy(chips2);

        assertEquals(chips1 + chips2, player.getAvailableChips());
    }

    @Test
    public void shouldJoinSameGameNoMoreThanSixPlayers() throws CasinoGameException {
        Player player2 = new Player();
        Player player3 = new Player();
        Player player4 = new Player();
        Player player5 = new Player();
        Player player6 = new Player();
        Player player7 = new Player();

        player.joins(game);
        player2.joins(game);
        player3.joins(game);
        player4.joins(game);
        player5.joins(game);
        player6.joins(game);
        assertThrows(CasinoGameException.class, () -> player7.joins(game));
    }

    @Test
    public void sixthPlayerShouldJoinSameGame() throws CasinoGameException {
        Player player2 = new Player();
        Player player3 = new Player();
        Player player4 = new Player();
        Player player5 = new Player();
        Player player6 = new Player();

        player.joins(game);
        player2.joins(game);
        player3.joins(game);
        player4.joins(game);
        player5.joins(game);
        player6.joins(game);
        player6.joins(game);
    }
}