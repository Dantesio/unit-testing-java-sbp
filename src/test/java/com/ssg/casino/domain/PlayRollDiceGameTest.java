package com.ssg.casino.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

public class PlayRollDiceGameTest {

    private final int score = 3;
    private final Dice dice = Mockito.mock(Dice.class);
    private final RollDiceGame game = new RollDiceGame(dice);
    private final Player player = Mockito.mock(Player.class);

    @BeforeEach
    public void setUp() {
        Mockito.when(dice.getScore()).thenReturn(score);
    }

    @Test
    public void shouldGamePlayCallPlayerWin6X() {
        Bet bet = new Bet(2, score);
        game.addBet(player, bet);

        game.play();
        Mockito.verify(player).win(6 * bet.getAmount());
    }

    @Test
    public void shouldGamePlayCallPlayerLose() {
        Bet bet = new Bet(2, score - 1);
        game.addBet(player, bet);

        game.play();
        Mockito.verify(player).lose();
    }
}
