package com.ssg.casino.domain;

import java.util.*;

public class RollDiceGame {
    private final Dice dice;

    private final Set<Player> participants = new HashSet<>();
    private final HashMap<Player, Bet> playersBets = new HashMap<>();

    public RollDiceGame() {
        this(() -> {
            Random random = new Random();
            return random.nextInt(6) + 1;
        });
    }

    public RollDiceGame(Dice dice) {
        this.dice = dice;
    }

    void addParticipant(Player player) throws CasinoGameException {
        if (participants.size() >= 6) {
            throw new CasinoGameException("Game must contain no more than six players");
        }
        participants.add(player);
    }

    public void addBet(Player player, Bet bet) {
        playersBets.put(player, bet);
    }

    public void play() {
        int winningScore = dice.getScore();

        for (Player player : playersBets.keySet()) {
            Bet bet = playersBets.get(player);
            if (bet.getScore() == winningScore) {
                player.win(bet.getAmount() * 6);
            } else {
                player.lose();
            }
        }
        playersBets.clear();
    }

    public void leave(Player player) {
        if (!playersBets.containsKey(player)) {
            return;
        }

        playersBets.remove(player);
    }

    public HashMap<Player, Bet> getPlayersBets() {
        return playersBets;
    }
}
