package com.ssg.casino.domain;

public interface Dice {

    int getScore();
}
